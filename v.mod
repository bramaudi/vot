Module {
	name: 'vot'
	description: 'Super simple blog engine written in V, inspired by bearblog.dev'
	version: '0.0.1'
	license: 'MIT'
	dependencies: ['markdown']
}
