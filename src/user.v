module main

import veb
import veb.auth
import time
import entities

// @[middleware: test]
@['/user']
pub fn (mut app App) r_user_index(mut ctx Context) veb.Result {
	users := app.services.user_get_all()
	title := 'Users'
	content := $tmpl('templates/pages/admin/user.html')
	layout := $tmpl('templates/layouts/base.html')
	return ctx.html(layout)
}

// @[middleware: use_admin]
@['/user/register'; post]
pub fn (mut app App) handle_user_register(mut ctx Context) veb.Result {
	username := ctx.form['username']
	password := ctx.form['password']
	vpassword := ctx.form['vpassword']
	if username.len == 0 || password.len == 0 || vpassword.len == 0 {
		return ctx.text('all input cannot be empty')
	}
	if vpassword != password {
		return ctx.text('password do not match')
	}
	app.services.user_add(username, password) or { ctx.error(err.str()) }
	return ctx.redirect('/user')
}

// @[middleware: use_admin]
@['/user/edit/:id'; post]
pub fn (mut app App) handle_user_edit(mut ctx Context, user_id int) veb.Result {
	mut user := app.services.user_get(user_id)
	username := ctx.form['username']
	if username.len == 0 {
		ctx.error('all field cannot be empty')
	}
	if user.id <= 0 {
		ctx.error('user not found')
	}
	if ctx.form_error.len == 0 {
		user.username = username
		app.services.user_edit(user, user_id) or { ctx.error(err.str()) }
		if ctx.form_error.len == 0 {
			return ctx.redirect('/user')
		}
	}
	return app.r_user_edit(mut ctx, user_id)
}

// @[middleware: use_admin]
@['/user/edit/:id']
pub fn (mut app App) r_user_edit(mut ctx Context, id int) veb.Result {
	user := app.services.user_get(id)
	title := 'Edit user: ${user.username}'
	content := $tmpl('templates/pages/admin/user-edit.html')
	layout := $tmpl('templates/layouts/base.html')
	return ctx.html(layout)
}

// @[middleware: use_admin]
@['/user/delete/:id']
pub fn (mut app App) r_user_delete(mut ctx Context, id int) veb.Result {
	app.services.user_delete(id) or { ctx.error(err.str()) }
	return ctx.redirect('/user')
}

@['/login'; post]
pub fn (mut app App) handle_user_login(mut ctx Context, username string, password string) veb.Result {
	user := app.services.user_get_by_username(username)
	if user.id == 0 {
		ctx.error('invalid username or password')
	}
	if !auth.compare_password_with_hash(password, user.salt, user.password) {
		ctx.error('invalid username or password')
	}
	if ctx.form_error.len == 0 {
		ip := ctx.ip()
		ua := ctx.req.header.get(.user_agent) or { '' }
		token := app.services.token_add(ip, ua, user.id) or { panic(err) }
		expire_date := time.now().add_days(200)
		ctx.set_cookie(name: 'token', value: token, path: '/', expires: expire_date)
		return ctx.redirect('/')
	}
	return app.r_user_login(mut ctx)
}

@['/login']
pub fn (mut app App) r_user_login(mut ctx Context) veb.Result {
	if app.user.id > 0 {
		ctx.redirect('/')
	}

	title := 'Login'
	content := $tmpl('templates/pages/admin/login.html')
	layout := $tmpl('templates/layouts/base.html')
	return ctx.html(layout)
}

@['/logout']
pub fn (mut app App) user_logout(mut ctx Context) veb.Result {
	token_cookie := ctx.get_cookie('token') or { '' }
	if token_cookie.len != 0 {
		ctx.set_cookie(name: 'token', value: '', path: '/')
		app.services.token_delete(token_cookie) or { ctx.error(err.str()) }
		app.user = entities.User{}
	}
	return ctx.redirect('/login')
}
