// Copyright (c) 2019-2021 Alexander Medvednikov. All rights reserved.
// Use of this source code is governed by a GPL license that can be found in the LICENSE file.
module services

import rand
import entities
import time

pub fn (mut ctx Services) token_add(ip string, ua string, user_id int) !string {
	mut uuid := rand.uuid_v4()
	token := entities.Token{
		user_id: user_id
		name: ua
		value: uuid
		ip: ip
	}
	sql ctx.db {
		insert token into entities.Token
	}!
	return uuid
}

pub fn (mut ctx Services) token_get(id int) entities.Token {
	tokens := sql ctx.db {
		select from entities.Token where id == id
	} or { []entities.Token{} }
	if tokens.len == 0 {
		return entities.Token{}
	}
	return tokens.first()
}

pub fn (mut ctx Services) token_get_by_value(value string) entities.Token {
	tokens := sql ctx.db {
		select from entities.Token where value == value
	} or { []entities.Token{} }
	if tokens.len == 0 {
		return entities.Token{}
	}
	token := tokens.first()
	sql ctx.db {
		update entities.Token set last_activity = time.now().str() where id == token.id
	} or {}
	return token
}

pub fn (mut ctx Services) token_get_all(user_id int) []entities.Token {
	tokens := sql ctx.db {
		select from entities.Token where user_id == user_id
	} or { []entities.Token{} }
	return tokens
}

pub fn (mut ctx Services) token_delete(token_cookie string) ! {
	sql ctx.db {
		delete from entities.Token where value == token_cookie
	}!
}
