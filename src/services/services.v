module services

import os
import entities
import db.sqlite

pub struct Services {
pub mut:
	db sqlite.DB
}

pub fn init() &Services {
	mut ctx := &Services{
		db: sqlite.connect('vot.db') or { panic(err) }
	}
	if os.file_size('vot.db') == 0 {
		ctx.db_init() or { panic(err) }
	}
	return ctx
}

pub fn (mut ctx Services) db_init() ! {
	sql ctx.db {
		create table entities.User
		create table entities.Token
		create table entities.Post
		create table entities.File
	}!
	ctx.user_init()
}
