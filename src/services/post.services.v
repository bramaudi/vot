module services

import regex
import entities
import time

pub fn (mut ctx Services) post_get_all(is_page bool) []entities.Post {
	mut posts := sql ctx.db {
		select from entities.Post where is_page == is_page
	} or { []entities.Post{} }
	return posts
}

pub fn (mut ctx Services) post_get_publish_all() []entities.Post {
	mut posts := sql ctx.db {
		select from entities.Post where is_page == false && draft == false
	} or { []entities.Post{} }
	return posts
}

pub fn (mut ctx Services) post_get(id int) entities.Post {
	posts := sql ctx.db {
		select from entities.Post where id == id
	} or { []entities.Post{} }
	if posts.len == 0 {
		return entities.Post{}
	}
	return posts.first()
}

pub fn (mut ctx Services) post_get_by_slug(slug string) entities.Post {
	posts := sql ctx.db {
		select from entities.Post where slug == slug
	} or { []entities.Post{} }
	if posts.len == 0 {
		return entities.Post{}
	}
	return posts.first()
}

pub fn (mut ctx Services) post_add(new_post entities.Post) ! {
	sql ctx.db {
		insert new_post into entities.Post
	}!
}

pub fn (mut ctx Services) post_edit(edited_post entities.Post, id int) ! {
	sql ctx.db {
		update entities.Post set title = edited_post.title, slug = edited_post.slug, content = edited_post.content,
		tags = edited_post.tags, draft = edited_post.draft, is_page = edited_post.is_page,
		updated_at = time.now().str() where id == id
	}!
}

pub fn (mut ctx Services) post_delete(post_id int) ! {
	sql ctx.db {
		delete from entities.Post where id == post_id
	}!
}

pub fn (mut ctx Services) post_slug_exists(slug string) bool {
	rows := sql ctx.db {
		select from entities.Post where slug == slug
	} or { []entities.Post{} }
	if rows.len > 0 && rows.first().id != 0 {
		return true
	}
	return false
}

pub fn (mut ctx Services) post_slug_duplicate(slug string, id int) bool {
	rows := sql ctx.db {
		select from entities.Post where slug == slug && id != id
	} or { []entities.Post{} }
	if rows.len == 0 {
		return false
	}
	return true
}

pub fn (mut ctx Services) post_slugify(title string) string {
	query := '[^0-9a-zA-Z]+'
	mut re := regex.regex_opt(query) or { panic(err) }
	slug := re.replace(title, ' ')
		.to_lower()
		.trim(' ')
		.split(' ')
		.join('-')
	return slug
}
