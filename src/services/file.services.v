module services

import entities

pub fn (mut ctx Services) file_get(file_id int) entities.File {
	rows := sql ctx.db {
		select from entities.File where id == file_id
	} or { []entities.File{} }
	if rows.len == 0 {
		return entities.File{}
	}
	return rows.first()
}

pub fn (mut ctx Services) file_get_all() []entities.File {
	rows := sql ctx.db {
		select from entities.File
	} or { []entities.File{} }
	return rows
}

pub fn (mut ctx Services) file_insert(file entities.File) ! {
	sql ctx.db {
		insert file into entities.File
	}!
}

pub fn (mut ctx Services) file_delete(file_id int) ! {
	sql ctx.db {
		delete from entities.File where id == file_id
	}!
}
