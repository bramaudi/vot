module services

import entities
import veb.auth

pub fn (mut ctx Services) user_init() {
	users := ctx.user_get_all()
	if users.len == 0 {
		ctx.user_add('admin', 'admin') or { dump(err.str()) }
	}
}

pub fn (mut ctx Services) user_get(id int) entities.User {
	rows := sql ctx.db {
		select from entities.User where id == id
	} or { []entities.User{} }
	if rows.len == 0 {
		return entities.User{}
	}
	return rows.first()
}

pub fn (mut ctx Services) user_get_all() []entities.User {
	rows := sql ctx.db {
		select from entities.User
	} or { []entities.User{} }
	return rows
}

pub fn (mut ctx Services) user_get_by_username(username string) entities.User {
	users := sql ctx.db {
		select from entities.User where username == username
	} or { []entities.User{} }
	if users.len == 0 {
		return entities.User{}
	}
	return users.first()
}

pub fn (mut ctx Services) user_add(username string, password string) ! {
	users := ctx.user_get_all()
	salt := auth.generate_salt()
	password_hash := auth.hash_password_with_salt(password, salt)
	new_user := entities.User{
		username: username
		password: password_hash
		salt: salt
		is_admin: if users.len == 0 { 1 } else { 0 }
	}
	sql ctx.db {
		insert new_user into entities.User
	}!
}

pub fn (mut ctx Services) user_delete(id int) ! {
	user := ctx.user_get(id)
	if user.is_admin == 0 {
		sql ctx.db {
			delete from entities.User where id == id
		}!
	}
}

pub fn (mut ctx Services) user_edit(user entities.User, id int) ! {
	sql ctx.db {
		update entities.User set username = user.username where id == id
	}!
}

pub fn (mut ctx Services) user_edit_pass(user entities.User, id int) ! {
	password_hash := auth.hash_password_with_salt(user.password, user.salt)
	sql ctx.db {
		update entities.User set password = password_hash where id == id
	}!
}

pub fn (mut ctx Services) user_check_is_banned(id int) bool {
	user := ctx.user_get(id)
	if user.is_banned == 1 {
		return true
	}
	return false
}
