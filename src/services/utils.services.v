module services

import math
import time
import markdown
import encoding.html

pub fn (mut app Services) utils_format_filesize(bytes int) string {
	if bytes == 0 {
		return '0 B'
	}
	k := 1024
	sizes := ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
	i := math.floor(math.log(bytes) / math.log(k))
	return '${f32(bytes / math.pow(k, i)):.2f} ${sizes[int(i)]}'
}

pub fn (mut app Services) utils_timesince(datetime string) string {
	now := time.now()
	t := time.parse(datetime) or { panic(err) }
	seconds := (now.unix() - t.unix())
	mut interval := seconds / 31536000
	if interval > 1 {
		return '${interval} years'
	}
	interval = seconds / 2592000
	if interval > 1 {
		return '${interval} months'
	}
	interval = seconds / 86400
	if interval > 1 {
		return '${interval} days'
	}
	interval = seconds / 3600
	if interval > 1 {
		return '${interval} hours'
	}
	interval = seconds / 60
	if interval > 1 {
		return '${interval} minutes'
	}
	return '${seconds} seconds'
}

pub fn (mut app Services) utils_format_date(datetime string, format string) string {
	date := time.parse(datetime) or { time.now() }
	return date.custom_format(format)
}

pub fn (mut app Services) utils_markdown_parse(content string) string {
	return markdown.to_html(content)
}

pub fn (mut app Services) utils_html_escape(content string) string {
	return html.escape(content)
}

pub fn (mut app Services) utils_read_time(content string) int {
	word_per_minute := 150
	words := content
		.split_into_lines()
		.filter(it.len > 0)
		.join(' ')
		.split(' ').len
	result := int(math.ceil(words / word_per_minute))
	return if result > 0 { result } else { 1 }
}
