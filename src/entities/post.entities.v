module entities

@[table: 'posts']
pub struct Post {
pub mut:
	id         int    @[primary; sql: serial]
	author_id  int
	title      string
	slug       string @[unique: 'posts']
	content    string @[sql_type: 'TEXT']
	tags       string
	draft      bool
	is_page    bool
	updated_at string @[default: 'CURRENT_TIMESTAMP']
	created_at string @[default: 'CURRENT_TIMESTAMP']
}
