module entities

@[table: 'users']
pub struct User {
pub mut:
	id            int    @[primary; sql: serial]
	username      string @[unique: 'users']
	fullname      string
	password      string
	salt          string
	is_admin      int    @[default: '0']
	is_banned     int    @[default: '0']
	last_activity string @[default: 'CURRENT_TIMESTAMP']
	created_at    string @[default: 'CURRENT_TIMESTAMP']
}
