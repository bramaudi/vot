module entities

@[table: 'tokens']
pub struct Token {
pub mut:
	id            int    @[primary; sql: serial]
	user_id       int
	name          string
	value         string
	ip            string
	last_activity string @[default: 'CURRENT_TIMESTAMP']
	created_at    string @[default: 'CURRENT_TIMESTAMP']
}
