module entities

@[table: 'files']
pub struct File {
pub mut:
	id           int    @[primary; sql: serial]
	user_id      int    @[nonnull]
	name         string @[nonnull]
	size         string @[nonnull]
	content_type string @[nonnull]
	metadata     string
	created_at   string @[default: 'CURRENT_TIMESTAMP']
}
