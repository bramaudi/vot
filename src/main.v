module main

import veb
import veb.auth
import entities
import services
import toml
import os
import db.sqlite

pub struct Context {
	veb.Context
}

pub struct App {
	veb.Middleware[Context]
	veb.StaticHandler
pub mut:
	services services.Services
	auth     auth.Auth[sqlite.DB]
	user     entities.User
}

fn main() {
	mut app := &App{}

	// initialize config file
	app.config('site.title')

	// setup services context
	app.services = services.init()
	app.auth = auth.new(app.services.db)

	// global middleware
	app.use(handler: app.update_auth_user)

	// post middleware
	app.route_use('/pages', handler: app.use_auth)
	app.route_use('/post/:path...', handler: app.use_auth)

	// profile middleware
	app.route_use('/profile', handler: app.use_auth)
	app.route_use('/profile/:path...', handler: app.use_auth)

	// files middleware
	app.route_use('/files', handler: app.use_auth)
	app.route_use('/files/delete/:id', handler: app.use_auth)

	// user middleware
	app.route_use('/user', handler: app.use_admin)
	app.route_use('/user/:path...', handler: app.use_admin)

	// serve folder named static to root path
	// example: ./static/main.js => host/main.js
	app.handle_static('static', true)!

	veb.run[App, Context](mut app, 8088)
}

pub fn (app &App) config(key string) string {
	simple_config := '# default config\n[site]\ntitle = "My site"\ndescription = "Powered by vot"'
	path := './config.toml'
	mut doc := toml.Doc{}
	if os.exists(path) {
		doc = toml.parse_file(path) or { panic(err) }
	} else {
		doc = toml.parse_text(simple_config) or { panic(err) }
		os.write_file(path, simple_config) or {}
	}
	return doc.value(key).string()
}

fn (mut app App) update_auth_user(mut ctx Context) bool {
	token_cookie := ctx.get_cookie('token') or { '' }
	if token_cookie == '' {
		app.user = entities.User{}
		return true
	}

	// verify token if client has token cookie
	token := app.services.token_get_by_value(token_cookie)
	if token.id == 0 {
		app.user = entities.User{}
		return true
	}

	app.user = app.services.user_get(token.user_id)
	return true
}

pub fn (mut app App) use_auth(mut ctx Context) bool {
	if app.services.user_check_is_banned(app.user.id) {
		ctx.error('user is banned')
		ctx.redirect('/login')
		return false
	}

	if app.user.id == 0 {
		ctx.redirect('/login')
		return false
	}

	return true
}

pub fn (mut app App) use_admin(mut ctx Context) bool {
	if app.user.is_admin == 0 {
		ctx.redirect('/')
		return false
	}

	return true
}

pub fn (app &App) not_found(mut ctx Context) veb.Result {
	ctx.res.set_status(.not_found)
	return ctx.text('404 Not Found')
}

@['/']
pub fn (mut app App) view_index(mut ctx Context) veb.Result {
	home_post := app.services.post_get_by_slug('home')
	if home_post.id == 0 || home_post.draft == true || home_post.is_page == false {
		return app.post_list(mut ctx)
	}
	return app.post_single(mut ctx, 'home')
}
