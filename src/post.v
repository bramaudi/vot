module main

import veb
import entities
import time

@['/blog']
pub fn (mut app App) post_list(mut ctx Context) veb.Result {
	title := app.config('site.title')
	is_page := ctx.query['is_page'].int() == 1
	mut posts := []entities.Post{}
	if app.user.id > 0 {
		posts = app.services.post_get_all(false).reverse()
	} else {
		posts = app.services.post_get_publish_all().reverse()
	}
	posts = posts.map(fn [mut app] (post entities.Post) entities.Post {
		mut p := post
		p.title = app.services.utils_markdown_parse(p.title)
		p.created_at = app.services.utils_format_date(post.created_at, 'DD MMM, YYYY')
		return p
	})
	content := $tmpl('templates/pages/post-list.html')
	layout := $tmpl('templates/layouts/base.html')
	return ctx.html(layout)
}

@['/blog.rss']
fn (mut app App) blog_rss(mut ctx Context) veb.Result {
	mut host := ctx.req.header.get(.host) or { '' }
	prefix := if host.contains('localhost') { '' } else { 's' }
	host = 'http${prefix}://${host}'
	posts := app.services.post_get_publish_all().reverse()
	content := $tmpl('templates/pages/rss.html')
	ctx.set_content_type('application/rss+xml')
	return ctx.ok(content)
}

@['/pages']
fn (mut app App) page_list(mut ctx Context) veb.Result {
	title := app.config('site.title')
	is_page := true
	posts := app.services.post_get_all(true)
	content := $tmpl('templates/pages/post-list.html')
	layout := $tmpl('templates/layouts/base.html')
	return ctx.html(layout)
}

@['/p/:slug']
pub fn (mut app App) post_single(mut ctx Context, slug string) veb.Result {
	mut post := app.services.post_get_by_slug(slug)
	if post.id == 0 || (post.draft == true && ctx.query['preview'].int() != 1) {
		return app.not_found(mut ctx)
	}
	title := post.title
	created_at := time.parse(post.created_at) or { panic(err) }
	updated_at := time.parse(post.updated_at) or { panic(err) }
	read_time := app.services.utils_read_time(post.content)
	post.title = app.services.utils_markdown_parse(post.title)
	post.content = app.services.utils_markdown_parse(post.content)
	post.created_at = app.services.utils_format_date(post.created_at, 'DD MMM, YYYY')
	post.updated_at = app.services.utils_format_date(post.updated_at, 'DD MMM, YYYY')
	show_last_modified := (updated_at.unix() - created_at.unix()) > 86400
	content := $tmpl('templates/pages/post.html')
	layout := $tmpl('templates/layouts/base.html')
	return ctx.html(layout)
}

@['/post/create'; post]
pub fn (mut app App) handle_post_create(mut ctx Context) veb.Result {
	title := ctx.form['title']
	mut slug := ctx.form['slug']
	tags := ctx.form['tags']
	draft := ctx.form['draft']
	content := ctx.form['content']
	is_page := ctx.form['is_page']
	created_at := time.now().as_local().str()
	if title.len == 0 {
		ctx.error('title cannot be empty')
	}
	if content.len == 0 {
		ctx.error('content cannot be empty')
	}
	if slug.len == 0 {
		slug = app.services.post_slugify(title)
	}
	if app.services.post_slug_exists(slug) {
		ctx.error('post slug already exists')
	}
	if ctx.form_error.len == 0 {
		new_post := entities.Post{
			title: title
			slug: slug
			content: content
			tags: tags
			author_id: app.user.id
			draft: draft.len != 0
			is_page: is_page.len != 0
			created_at: created_at
		}
		app.services.post_add(new_post) or { ctx.error(err.str()) }
		if ctx.form_error.len == 0 {
			return ctx.redirect('/')
		}
	}
	return app.post_create(mut ctx)
}

@['/post/create']
pub fn (mut app App) post_create(mut ctx Context) veb.Result {
	title := 'Posts'
	content := $tmpl('templates/pages/admin/post-create.html')
	layout := $tmpl('templates/layouts/base.html')
	return ctx.html(layout)
}

@['/post/:id'; post]
pub fn (mut app App) handle_post_edit(mut ctx Context, post_id int) veb.Result {
	title := ctx.form['title']
	mut slug := ctx.form['slug']
	tags := ctx.form['tags']
	draft := ctx.form['draft']
	content := ctx.form['content']
	is_page := ctx.form['is_page']
	if title.len == 0 {
		ctx.error('title cannot be empty')
	}
	if content.len == 0 {
		ctx.error('content cannot be empty')
	}
	if slug.len == 0 {
		slug = app.services.post_slugify(title)
	}
	if app.services.post_slug_duplicate(slug, post_id) {
		ctx.error('post slug already exists')
	}
	if ctx.form_error.len == 0 {
		edited_post := entities.Post{
			title: title
			slug: slug
			content: content
			tags: tags
			draft: draft.len != 0
			is_page: is_page.len != 0
		}
		// TODO: validate only author can edit
		app.services.post_edit(edited_post, post_id) or { ctx.error(err.str()) }
	}
	return app.post_edit(mut ctx, post_id)
}

@['/post/:id']
pub fn (mut app App) post_edit(mut ctx Context, id int) veb.Result {
	post := app.services.post_get(id)
	title := 'Posts'
	content := $tmpl('templates/pages/admin/post-edit.html')
	layout := $tmpl('templates/layouts/base.html')
	return ctx.html(layout)
}

@['/post/delete/:id']
pub fn (mut app App) post_delete(mut ctx Context, id int) veb.Result {
	// TODO: validate only author can delete
	app.services.post_delete(id) or { ctx.error(err.str()) }
	return ctx.redirect('/')
}
