module main

import veb
import veb.auth
import entities

@['/profile']
pub fn (mut app App) r_profile_index(mut ctx Context) veb.Result {
	title := 'Profile'
	token := ctx.get_cookie('token') or { '' }
	mut tokens := app.services.token_get_all(app.user.id)
	tokens = tokens.map(fn [mut app] (token entities.Token) entities.Token {
		mut t := token
		t.last_activity = app.services.utils_timesince(token.last_activity)
		return t
	})
	content := $tmpl('templates/pages/admin/profile.html')
	layout := $tmpl('templates/layouts/base.html')
	return ctx.html(layout)
}

@['/profile/change_pass'; post]
pub fn (mut app App) handle_profile_change_pass(mut ctx Context) veb.Result {
	oldpass := ctx.form['oldpass']
	pass := ctx.form['pass']
	vpass := ctx.form['vpass']
	if oldpass.len == 0 || pass.len == 0 || vpass.len == 0 {
		ctx.error('all field cannot be empty')
	}
	if pass != vpass {
		ctx.error('new password did not match')
	}
	if !auth.compare_password_with_hash(oldpass, app.user.salt, app.user.password) {
		ctx.error('wrong old password')
	}
	if ctx.form_error.len == 0 {
		mut user := app.user
		user.password = pass
		app.services.user_edit_pass(user, app.user.id) or { ctx.error(err.str()) }
		if ctx.form_error.len == 0 {
			return ctx.redirect('/profile')
		}
	}
	return app.r_profile_index(mut ctx)
}

@['/profile/session_end_all']
pub fn (mut app App) r_profile_session_end_all(mut ctx Context) veb.Result {
	current_token := ctx.get_cookie('token') or { '' }
	tokens := app.services.token_get_all(app.user.id)
	for token in tokens {
		if token.value != current_token {
			app.services.token_delete(token.value) or { ctx.error(err.str()) }
		}
	}
	return ctx.redirect('/profile')
}

@['/profile/session_end/:id']
pub fn (mut app App) r_profile_session_end(mut ctx Context, id int) veb.Result {
	token := app.services.token_get(id)
	app.services.token_delete(token.value) or { ctx.error(err.str()) }
	return ctx.redirect('/profile')
}
