module main

import veb
import os
import entities

fn files_dir() string {
	path := './files'
	if !os.exists(path) {
		os.mkdir(path) or {}
	}
	return path
}

@['/files/:path…']
pub fn (mut app App) r_files(mut ctx Context, path string) veb.Result {
	f_path := '${files_dir()}/${path}'
	if !os.exists(f_path) {
		return app.not_found(mut ctx)
	}
	ext := os.file_ext(f_path)
	data := os.read_file(f_path) or { panic(err) }
	content_type := veb.mime_types[ext]
	if content_type.len == 0 {
		ctx.set_header(.content_disposition, 'attachment; filename=${os.base(f_path)}')
		return ctx.send_response_to_client('application/octet-stream', data)
	} else {
		return ctx.send_response_to_client(content_type, data)
	}
}

@['/files']
pub fn (mut app App) r_file_index(mut ctx Context) veb.Result {
	// TODO: only show owned posts except for admin
	files := app.services.file_get_all().reverse()
	title := 'Files'
	content := $tmpl('templates/pages/admin/file.html')
	layout := $tmpl('templates/layouts/base.html')
	return ctx.html(layout)
}

@['/files'; post]
pub fn (mut app App) handle_file_upload(mut ctx Context) veb.Result {
	fdata := ctx.files['files']
	for d in fdata {
		path := '${files_dir()}/${d.filename}'
		if d.filename.len == 0 {
			ctx.error('Please select a file')
		}
		if d.filename.len > 0 && os.exists(path) {
			ctx.error('File name already exists')
		}
		if ctx.form_error.len == 0 {
			os.write_file(path, d.data) or { ctx.error(err.str()) }
			filedata := entities.File{
				user_id: app.user.id
				name: d.filename
				size: d.data.len.str()
				content_type: d.content_type
			}
			app.services.file_insert(filedata) or {
				// if error on db then delete file in disk
				ctx.error(err.str())
				os.rm(path) or {}
			}
			if ctx.form_error.len == 0 {
				return ctx.redirect('/files')
			}
		}
	}
	return app.r_file_index(mut ctx)
}

@['/files/delete/:id']
pub fn (mut app App) r_file_delete(mut ctx Context, file_id int) veb.Result {
	// TODO: validate only author can edit
	file := app.services.file_get(file_id)
	if file.id > 0 {
		app.services.file_delete(file_id) or { ctx.error(err.str()) }
		os.rm('${files_dir()}/${file.name}') or { ctx.error(err.str()) }
	}
	return ctx.redirect('/files')
}
