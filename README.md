# vot

A small single binary to run your very simple blog.

## Features

- Manage **blog** post
- Manage **pages** post
- Manage files
- Manage users (admin role only)
- Multi-user for single blog
- RSS Feed

## Configuration

### Homepage

To enable home page you can add new page with slug `home`.

### Site variable

Edit `config.toml` to change page title and description.

### Serve static files

- `static` - all files inside this folder will accessible in your root path, example: `static/style.css` => domain.com/style.css
- `files` - same as `static` folder but this are where files from upload are stored and need `/files` prefix path.

## Installation

> Currently only supporting for Linux, for other OS you can compile by yourself.

### Release binary

Download latest binary in [Release](https://codeberg.org/bramaudi/vot/releases) page, extract and run.

### Docker

1. Create a new project folder
2. Make sure executable binary already exists inside project folder, you can get by downloading from [Release](https://codeberg.org/bramaudi/vot/releases) page or build from source; see Development section below
3. Download & move the `docker-compose.yml`,`Dockerfile`, and `config.toml` into the project folder
4. Make new folder inside project folder named `static` and `files`
5. Create new empty file named `vot.db` in project folder
6. Execute command `docker compose up -d --build`

## Development

Install common **C** builder and needed libs.

Debian-based: `sudo apt install build-essential libsqlite3-dev`

Install V dependencies:
```
v install
```

Running development server:
```
./dev
```

Build production binary:
```
./build
```

Testing production binary, this script just simplify the build then running proccess:
```
./make_test
``` 